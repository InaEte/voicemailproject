import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;


class _MailboxTest {
	
	Mailbox mailbox;
	Message message;
	@BeforeEach
	void setup() {
		 mailbox= new Mailbox("12","Welcome");
		 message =new Message("Good evening, I am calling from the bank");
	}

	@Test
	void sendingCorrectPasscodeShouldreturnTrue() {
		assertEquals(mailbox.checkPasscode("12"),true);
	}
	@Test
	void sendingWrongPasscodeShouldreturnFalse() {
		assertEquals(mailbox.checkPasscode("1"),false);
	}

	@Test
	void addNewMessageShouldAddCurrentMessage() {
		mailbox.addMessage(message);
		assertEquals(mailbox.getCurrentMessage(),message);	
	}
	
	@Test
	void noCurrentMessageShouldReturnNull() {
		assertEquals(mailbox.getCurrentMessage(),null);
	}
	@Test
	
	void peekLastKeptMessageShouldReturnKeptMessage() {
		mailbox.addMessage(message);
		mailbox.saveCurrentMessage();
		
		assertEquals(mailbox.getCurrentMessage(),message);
	}

	@Test
	void removeMessageShouldNotApperarAgainInCurrentMessage() {
		mailbox.addMessage(message);
		assertEquals(mailbox.getCurrentMessage(),message);
		mailbox.removeCurrentMessage();
		assertEquals(mailbox.getCurrentMessage(),null);
	}
	@Test
	void removeMessageShouldNotApperarAgainInKeptMessage() {
		mailbox.addMessage(message);
		mailbox.saveCurrentMessage();
		assertEquals(mailbox.getCurrentMessage(),message);
		mailbox.removeCurrentMessage();
		assertEquals(mailbox.getCurrentMessage(),null);
	}
	@Test
	void removeMessageWhenThereIsNoMessageShouldReturnNull() {
		assertEquals(mailbox.removeCurrentMessage(),null);
	}

	@Test
	void setGretingShouldSetNewGreeting() {
		mailbox.setGreeting("New greeting");
		assertEquals(mailbox.getGreeting(),"New greeting");
	}
	@Test
	void setPasscodeShouldSetNewPasscode() {
		mailbox.setPasscode("123");
		assertEquals(mailbox.checkPasscode("123"),true);
	}

}
