import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class _MessageQueueTest {

	MessageQueue queue;
	Message message;
	@BeforeEach
	public void setup() {
		 queue = new MessageQueue();
		 message= new Message("Example Message");
	}
	@Test
	void whenMessageIsRemovedSizeShouldDecrease() {
		
		queue.add(message);
		queue.remove();
		assertEquals(0,queue.size());
		
	}


	@Test
	void sizeOfQueueShouldBeZero() {
		assertEquals(0,queue.size());
	}
	@Test
	void sizeOfQueueShouldBeOne() {
		queue.add(message);
		assertEquals(1,queue.size());
	}

	@Test
	void peekOfEmptyQueueShouldReturnNull() {
		assertEquals(null,queue.peek());
	}
	@Test
	void peekShouldReturnLastMessage() {
		queue.add(message);
		assertEquals(message,queue.peek());
	}

}
