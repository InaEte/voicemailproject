

import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DesktopTelephone implements Telephone {

	private JFrame frame;
	private JTextPane txtpnInstructions;
	private JTextArea textArea;
	private JButton btnSend;
	private JButton btnHangUp;
	private JButton btnOne;
	private JButton btnTwo;
	private JButton btnThree;
	private JButton btnFour;
	private JButton btnFive;
	private JButton btnSix;
	private JButton btnSeven;
	private JButton btnEight;
	private JButton btnNine;
	private JButton btnZero;
	
	
	String userInput="";
	String connectionMessage;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DesktopTelephone window = new DesktopTelephone();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	

	/**
	 * Create the application.
	 */
	public DesktopTelephone() {
		initialize();
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		txtpnInstructions = new JTextPane();
		textArea = new JTextArea();
		btnSend = new JButton("Send");
		btnHangUp = new JButton("Hang Up");
		btnOne = new JButton("1");
		btnTwo = new JButton("2");
		btnThree = new JButton("3");
		btnFour = new JButton("4");
		btnFive = new JButton("5");
		btnSix = new JButton("6");
		btnSeven = new JButton("7");
		btnEight = new JButton("8");
		btnNine = new JButton("9");
		btnZero = new JButton("0");
		
		
		
		frame.setBounds(100, 100, 570, 434);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblVoicemail = new JLabel("VoiceMail");
		lblVoicemail.setBounds(169, 16, 69, 20);
		frame.getContentPane().add(lblVoicemail);
		
		
		txtpnInstructions.setText("Enter mailbox number followed by send");
		txtpnInstructions.setBounds(253, 103, 245, 208);
		txtpnInstructions.setEditable(false);
		frame.getContentPane().add(txtpnInstructions);
		 
		textArea.setBounds(15, 44, 223, 22);
		frame.getContentPane().add(textArea);
		
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				
				System.out.println(textArea.getText());
				if(textArea.getText().equals("1")) {
					txtpnInstructions.setText("You have reached mailbox " + textArea.getText() +
							"\n Please leave a message now and hang up");
					userInput="";	
				}
				else {
					txtpnInstructions.setText("Incorrect mailbox number. Try again!");
					userInput="";
				}
				
				
				textArea.setText(userInput);
			}
		});
		
		btnSend.setBounds(253, 40, 115, 29);
		frame.getContentPane().add(btnSend);
		
		btnHangUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				userInput="";
				userInput=textArea.getText();
				txtpnInstructions.setText("Dejo el mensaje:"+userInput);
				userInput="";
			}
		});
		btnHangUp.setBounds(383, 40, 115, 29);
		frame.getContentPane().add(btnHangUp);
			
		btnOne.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
			
				btnOneAction();
			}
		});
		btnOne.setBounds(15, 103, 60, 40);
		frame.getContentPane().add(btnOne);
		
		btnTwo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				btnTwoAction();
			}
		});
		btnTwo.setBounds(90, 103, 60, 40);
		frame.getContentPane().add(btnTwo);	
			
		btnThree.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				btnThreeAction();
				
			}
		});
		btnThree.setBounds(169, 103, 60, 40);
		frame.getContentPane().add(btnThree);
		
		btnFour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				btnFourAction();
			}
		});
		btnFour.setBounds(15, 159, 60, 40);
		frame.getContentPane().add(btnFour);
		
		btnFive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				btnFiveAction();
			}
		});
		btnFive.setBounds(90, 159, 60, 40);
		frame.getContentPane().add(btnFive);
		
		btnSix.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				btnSixAction();
			}
		});
		btnSix.setBounds(169, 159, 60, 40);
		frame.getContentPane().add(btnSix);
		
		btnSeven.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				btnSevenAction();
			}
		});
		btnSeven.setBounds(15, 215, 60, 40);
		frame.getContentPane().add(btnSeven);
		
		btnEight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				btnEightAction();
			}
		});
		btnEight.setBounds(90, 215, 60, 40);
		frame.getContentPane().add(btnEight);
		
		btnNine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				btnNineAction();
			}
		});
		btnNine.setBounds(169, 215, 60, 40);
		frame.getContentPane().add(btnNine);
		
		btnZero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				btnZeroAction();
			}
		});
		btnZero.setBounds(90, 271, 60, 40);
		frame.getContentPane().add(btnZero);
			
	}
	
	private void btnZeroAction () {
		userInput+="0";
		textArea.setText(userInput);	
	}
	
	private void btnOneAction() {
		userInput=userInput+"1";
		textArea.setText(userInput);
	}
	
	private void btnTwoAction() {
		userInput+="2";
		textArea.setText(userInput);
	}

	private void btnThreeAction() {
		userInput+="3";
		textArea.setText(userInput);	
	}
	
	private void btnFourAction() {
		userInput=userInput+"4";
		textArea.setText(userInput);
	}
	
	private void btnFiveAction() {
		userInput+="5";
		textArea.setText(userInput);
	}

	private void btnSixAction() {
		userInput+="6";
		textArea.setText(userInput);	
	}
	
	private void btnSevenAction() {
		userInput+="7";
		textArea.setText(userInput);
	}
	
	private void btnEightAction() {
		userInput+="8";
		textArea.setText(userInput);
	}
	private void btnNineAction() {
		userInput+="9";
		textArea.setText(userInput);
	}
	
	
	@Override
	public void speak(String message) {
	
		connectionMessage = message;
		txtpnInstructions.setText(connectionMessage);
		
	}


	@Override
	public void run(Connection connection) {
		//TO DO
		
	}
}
