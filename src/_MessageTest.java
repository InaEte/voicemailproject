import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class _MessageTest {

	@Test
	void testGetText() {
		Message message =  new Message("Message content here");
		assertEquals(message.getText(),"Message content here");
	}

}
