
public interface Telephone {
	void speak(String message);
	void run(Connection connection);
}
