import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;




public class _ConnectionTest {
	MailSystem system;
	ConsoleTelephone phone;
	Connection connection;
	Mailbox mailbox;
	Message message;
	Telephone phones[];
	@Before
	public void setup() {
	 system = mock(MailSystem.class);
	 phone = mock(ConsoleTelephone.class);
	 phones = mock(Telephone.class)[];
	 connection = new Connection(system, phone,phones);
	 mailbox = mock(Mailbox.class);	
	 message= mock(Message.class);
	}
	@Test
	public void connectionShouldStartWithInitialPrompt() {
		verify(phone).speak("Enter mailbox number followed by #");
	}

	@Test
	public void dialExistingMailbox() {
    
    when(system.findMailbox("1")).thenReturn(mailbox);
    when(mailbox.getGreeting()).thenReturn("Hola :");
    connection.dial("1");
    connection.dial("#");
    verify(phone).speak("Hola :");
    
	}

	@Test
	public void dialIncorrectMailBoxShouldFail() {   
    	when(system.findMailbox("100")).thenReturn(null);
    	connection.dial("1");
    	connection.dial("0");
    	connection.dial("0");
    	connection.dial("#");
    	verify(phone).speak("Incorrect mailbox number. Try again!");
	}
	
	@Test
	public void dialWithCorrectPasscode() {
    	
    	when(system.findMailbox("1")).thenReturn(mailbox);
    	when(mailbox.checkPasscode("1")).thenReturn(true);
    	connection.dial("1");
    	connection.dial("#");
    	connection.dial("1");
    	connection.dial("#");
    	verify(phone).speak(  "Enter 1 to listen to your messages\n"
    	         + "Enter 2 to change your passcode\n"
    	         + "Enter 3 to change your greeting");
	}
	
	@Test
	public void dialWithIncorrectPasscode() {
		when(system.findMailbox("1")).thenReturn(mailbox);
		when(mailbox.checkPasscode("2")).thenReturn(false);
		connection.dial("1");
    	connection.dial("#");
    	connection.dial("2");
    	connection.dial("#");
    	verify(phone).speak("Incorrect passcode. Try again!");
	}
	
	@Test
	public void changePasscodeShouldCallSetPasscode() {
		when(system.findMailbox("1")).thenReturn(mailbox);
    	when(mailbox.checkPasscode("1")).thenReturn(true);
    	connection.dial("1");
    	connection.dial("#");
    	connection.dial("1");
    	connection.dial("#");
    	connection.dial("2");
    	connection.dial("4");
    	connection.dial("#");
    	verify(mailbox).setPasscode("4");
	}
	
	@Test
	public void changeGreetingShouldCallSetGreeting() {
		when(system.findMailbox("1")).thenReturn(mailbox);
		when(mailbox.checkPasscode("1")).thenReturn(true);
		connection.dial("1");
    	connection.dial("#");
    	connection.dial("1");
    	connection.dial("#");
    	connection.dial("3");
    	connection.record("Hola");
    	connection.dial("#");
    	verify(mailbox).setGreeting("Hola");	
	}
	@Test 
	public void showMailboxMenu() {
		when(system.findMailbox("1")).thenReturn(mailbox);
		when(mailbox.checkPasscode("1")).thenReturn(true);
		connection.dial("1");
    	connection.dial("#");
    	connection.dial("1");
    	connection.dial("#");
    	connection.dial("1");
    	verify(phone).speak("Enter 1 to listen to the current message\n"
    	         + "Enter 2 to save the current message\n"
    	         + "Enter 3 to delete the current message\n"
    	         + "Enter 4 to return to the main menu");
	}
	@Test 
	public void listenCurrentMessage(){
		when(system.findMailbox("1")).thenReturn(mailbox);
		when(mailbox.checkPasscode("1")).thenReturn(true);
		
		connection.dial("1");
    	connection.dial("#");
    	connection.dial("1");
    	connection.dial("#");
    	connection.dial("1");
    	connection.dial("1");
    	verify(mailbox).getCurrentMessage();
	}
	@Test 
	public void saveCurrentMessage(){
		when(system.findMailbox("1")).thenReturn(mailbox);
		when(mailbox.checkPasscode("1")).thenReturn(true);
		connection.dial("1");
    	connection.dial("#");
    	connection.dial("1");
    	connection.dial("#");
    	connection.dial("1");
    	connection.dial("2");
    	verify(mailbox).saveCurrentMessage();
	}
	@Test 
	public void deleteCurrentMessage(){
		when(system.findMailbox("1")).thenReturn(mailbox);
		when(mailbox.checkPasscode("1")).thenReturn(true);
		connection.dial("1");
    	connection.dial("#");
    	connection.dial("1");
    	connection.dial("#");
    	connection.dial("1");
    	connection.dial("3");
    	verify(mailbox).removeCurrentMessage();
	}
	@Test 
	public void returnToMainMenu(){
		when(system.findMailbox("1")).thenReturn(mailbox);
		when(mailbox.checkPasscode("1")).thenReturn(true);
		connection.dial("1");
    	connection.dial("#");
    	connection.dial("1");
    	connection.dial("#");
    	connection.dial("1");
    	connection.dial("4");
    	verify(phone,times(2)).speak(  "Enter 1 to listen to your messages\n"
   	         + "Enter 2 to change your passcode\n"
   	         + "Enter 3 to change your greeting");
    	
	}
	@Test 
	public void shouldHangup(){
		String recording= "Te llamo mas tarde";
		when(system.findMailbox("1")).thenReturn(mailbox);
		connection.dial("1");
    	connection.dial("#");;
    	connection.record(recording);
    	connection.hangup();
    	verify(phone,times(2)).speak("Enter mailbox number followed by #");
	}

}
